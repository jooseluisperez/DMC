service mysql start
mysql -u root -e "CREATE USER 'root'@'%' IDENTIFIED BY 'jupiter';"
mysql -u root -e "GRANT ALL ON *.* TO 'root'@'%';"
mysql -h localhost -u root < /opt/docker/nextcloud.sql
mysql -u root -e "CREATE USER 'oc_Admin'@'%' IDENTIFIED BY '2Zumi6p3GdpYWcs9PNuSAMyU+KaD5q';"
mysql -u root -e "GRANT ALL PRIVILEGES ON nextcloud . * TO 'oc_Admin'@'%';"
mysql -h localhost -u root < /opt/docker/ampache.sql
mysql -u root -e "CREATE USER 'amp_adm'@'%' IDENTIFIED BY 'amppass';"
mysql -u root -e "GRANT ALL PRIVILEGES ON ampache . * TO 'amp_adm'@'%';"
service mysql stop
/bin/sh /usr/bin/mysqld_safe
