# Docker Media Center (DMC) Project

El proyecto consta en implementar una estructura de contenedores que ofrezcan servicios de descarga/almacenamiento de archivos, streaming de audio/video y videollamadas en tiempo real. Una de las principales virtudes del proyecto es implementar una autenticación compartida entre todos los servicios mediante LDAP de forma que con las mismas credenciales se pueda a acceder a los tres servicios sin problema alguno.

* Tecnologías: Nexcloud, Ampache, JITSI, MySQL, LDAP.
* Autor: José Luis Pérez Sánchez.
* Motivo: Proyecto de fin de curso EDT ASIX 2018.
* Fechas: 16 de abril de 2018 - 20 de mayo de 2018.


