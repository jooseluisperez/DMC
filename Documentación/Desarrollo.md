# Docker Media Center (DMC) Project - Desarrollo

El proyecto consta en implementar una estructura de contenedores que sirvan ciertas aplicaciones de descarga y almacenamiento de ficheros, streaming de video y audio y videollamadas compartiendo entre ellas autenticación con LDAP.

- [Docker Media Center (DMC) Project - Desarrollo](#docker-media-center-dmc-project---desarrollo)
  * [Configuración previa](#configuracion-previa)
  * [Servicios utilizados](#servicios-utilizados)
    + [Descarga y almacenamiento de archivos](#descarga-y-almacenamiento-de-archivos)
      - [Entorno de pruebas](#entorno-de-pruebas)
      - [Estructura interna](#estructura-interna)
      - [Indexación de documentos](#indexacion-de-documentos)
      - [Creación del dockerfile de Nextcloud](#creacion-del-dockerfile-de-nextcloud)
    + [Transmisión de audio/video bajo demanda](#transmision-de-audiovideo-bajo-demanda)
      - [Entorno de pruebas](#entorno-de-pruebas-1)
      - [Configuración de LDAP](#configuracion-de-ldap)
      - [Personalización del theme](#personalizacion-del-theme)
      - [Carga del catálogo de audio y video](#carga-del-catalogo-de-audio-y-video)
      - [Carga de feeds de radio](#carga-de-feeds-de-radio)
      - [Análisis de tráfico](#analisis-de-trafico)
      - [Modos de transmisión de contenido](#modos-de-transmision-de-contenido)
      - [Creación del contenedor final](#creacion-del-contenedor-final)
    + [Video-llamadas en directo](#video-llamadas-en-directo)

## Configuración previa

Para varias de las tareas ejecutadas se ha tenido que instalar la útima versión de Docker-CE y docker-compose. Para ello tendríamos que seguir los pasos indicados en las siguientes guías:

https://docs.docker.com/compose/install/#install-compose

https://docs.docker.com/install/linux/docker-ce/fedora/#install-docker-ce-1

## Servicios utilizados

### Descarga y almacenamiento de archivos

Para dar servicio a esta tarea se ha decidido utilizar **Nexcloud**. Este software nació a raíz de un fork de owncloud y gracias a un constante desarrollo es
una de las mejores soluciones a la hora de implementar un sistema de almacenamiento de datos compartido.

El entorno que necesita Nextcloud a parte del propio aplicativo se forma por una base de datos (sqlite,MySQL o PgSQL)  en la que indexa los ficheros y configuraciones necesarias para el correcto funcionamiento de este.

Se ha decidido separar en dos contenedores los servicios para utilizar la base de datos con otros servicios que requieran de esta para funcionar.

#### Entorno de pruebas

Para conocer como funciona Nextcloud detrás del telón se ha montado un entorno de pruebas en contenedores de la modo que posteriormente se puedan generar
los contenedores finales configurados a nuestro gusto.

**Contenedor de MySQL**

Para este paso hemos lanzado un contenedor preconfigurado de los [repositorios oficiales](https://hub.docker.com/_/mysql/) pero hemos tenido que utilizar 
una versión inferior dado a que, si se utilizaba la última (mysql:latest), la autenicación que utilizaba el cliente tenía problemas con la que esperaba el servidor.

    docker run --name mysql --restart unless-stopped -p 3306:3306 -e MYSQL_ROOT_PASSWORD='jupiter' -d mysql:5.6
    
**Contenedor de Nextcloud**

De la misma forma que con MySQL se ha utlizado un contenedor de los [repositorios oficiales](https://hub.docker.com/_/nextcloud/) de Nextcloud para agilizar 
 el proceso de testing y evitar problemas de configuración básica del servicio.

	docker run --name nextcloud -h nextcloud --restart unless-stopped -d -p 8080:80 nextcloud

**Contenedor de LDAP**

Para el servicio de LDAP se ha utilizado un [contenedor de pruebas](https://hub.docker.com/r/edtasixm11/ldapserver/) que se ha utilizado ampliamente en los 
ejercicios de clase.

	docker run -it --name ldapserver -h ldapserver edtasixm06/ldapserver:dataDB /bin/bash

Es necesario acceder al contenedor y arrancar el servicio LDAP de la siguiente manera:

	docker exec -it ldapserver /bin/bash
	cd /opt/docker
	./startup.sh

Llegados a este punto, ya tenemos estamos listos para investigar como funciona nextcloud. Podemos acceder al configurador de Nextcloud mediante la siguiente url:

	localhost:8080
	
Acto seguido configuraremos el usuario administrador y toda la información referente a la BBDD.

![](../Imágenes/nextcloud_config.png)

Finalizada esta configuración, vamos a añadir el módulo necesario para poder realizar la autenticación LDAP en Nextcloud. Para ello nos dirigimos en el menú de la esquina superior derecha a "Apps" y localizamos el módulo "LDAP user and group backend" y lo habilitamos.

![](../Imágenes/nex_ldap_module.png)

A continuación procedemos a configurar el módulo para apuntar a nuestro servidor LDAP. Dicha configuración se encuentra en el mismo menú localizado en la esquina superior derecha en "Settings" > "LDAP / AD integration". La configuración aplicada es la siguiente.
 
Podemos encontrar más información oficial respecto al proceso de configuración del módulo de LDAP en el siguiente [enlace](https://docs.nextcloud.com/server/12/admin_manual/configuration_user/user_auth_ldap.html).
 
![](../Imágenes/nex_ldap_server.png)

![](../Imágenes/nex_ldap_users.png)

![](../Imágenes/nex_ldap_login.png)

Después de esta configuración, intentamos hacer un login pero no conseguiamos iniciar sesión con un usuario de LDAP ya que Nexctloud quedaba procesando un tiempo hasta responder un *Internal server error*. Para poder dar solución al problema se probó arrancando el servidor ldap en modo *debug* mostrando todo el tráfico LDAP que atendía y monitorizando el log de nextcloud en búsqueda de un posible problema en la configuración de LDAP.

Tras un par de días, descartando que el problema pudiese venir por la configuración de LDAP y realizando una pequeña búsqueda por internet, encontramos el siguiente [enlace](https://github.com/nextcloud/server/issues/6956). En el se plantea un problema el cual se acerca bastante al nuestro y se enfoca la solución hacia el servicio de caché que utiliza Nextcloud. Revisando el fichero de configuración de Nextcloud, encuentro que no se está utilizando el mismo sistema de caché que se comenta en el enlace pero cambiando la ruta de caché solucionamos el problema.

	$CONFIG = array (
	'htaccess.RewriteBase' => '/',
	'memcache.local' => '\OC\Memcache\APCu',
	'apps_paths' => 

Ahora se puede comprobar que es posible realizar un inicio de sesión con los usuarios de LDAP y que todos entre ellos se ven en Nextcloud. Esto es bueno ya que puede utilizarse junto con otras funciones de Nextcloud como la compartición de calendarios o la restricción por grupos para acceder a diversos directorios. Todo esto a gusto del administrador.

![](../Imágenes/nex_login.png)

#### Estructura interna

Toda la estructura de directorios que forma Nextcloud forma a partir del directorio '/var/www/html' y tiene la siguiente forma:

	/var/www/html
	|-- 3rdparty
	|   |-- [...]
	|-- AUTHORS
	|-- apps
	|   |-- theming
	|   |-- twofactor_backupcodes
	|   |-- updatenotification
	|   |-- user_external
	|   |-- user_ldap
	|   |-- ...
	|-- config
	|   |-- apache-pretty-urls.config.php
	|   |-- apcu.config.php
	|   |-- apps.config.php
	|   |-- config.php
	|   |-- ...
	|-- console.php
	|-- core
	|   |-- [...]
	|-- cron.php
	|-- custom_apps
	|   `-- bruteforcesettings
	|-- data
	|   |-- 5d07b76e-95ee-1037-8d37-db438ea12f92
	|   |-- 5d07fb48-95ee-1037-8d39-db438ea12f92
	|   |-- appdata_ociygmbslcn2
	|   |-- carlos
	|   |-- files_external
	|   |-- index.html
	|   `-- nextcloud.log
	|-- index.html
	|-- index.php
	|-- lib
	|   |-- [...]
	|-- occ
	|-- ocs
	|   |-- [...]
	|-- ocs-provider
	|   `-- [...]
	|-- public.php
	|-- remote.php
	|-- resources
	|   |-- [...]
	|-- robots.txt
	|-- settings
	|   |-- [...]
	|-- status.php
	|-- themes
	|   |-- [...]
	`-- version.php

De toda esta, los directorios más importantes son /data/ y /config/. 

Dentro de el directorio /data/ la información más relevante a destacar es la siguiente:

	data/
	|-- 5d07fb48-95ee-1037-8d39-db438ea12f92
	|   |-- cache
	|   `-- files
	|-- appdata_ociygmbslcn2
	|   |-- appstore
	|   |-- avatar
	|   |-- css
	|   |-- dav-photocache
	|   |-- js
	|   |-- preview
	|   `-- theming
	|-- carlos
	|   `-- files
	|-- files_external
	|   `-- rootcerts.crt
	|-- index.html
	`-- nextcloud.log

* Directorios de los usuarios

Cada vez que iniciemos sesión por primera vez con un usuario de LDAP, se creará un directorio con el UUID del usuario y dentro de este un directorio de cache y ficheros.
Dentro de este directorio de ficheros podemos acceder directamente a los datos del usuario.

	data/5d07fb48-95ee-1037-8d39-db438ea12f92/
	|-- cache
	`-- files
		|-- Documents
		|-- Nextcloud\ Manual.pdf
		|-- Nextcloud.mp4
		`-- Photos


* Fichero de log de Nextcloud

Nextcloud almacena el registro de todos los eventos referentes a este en este fichero por lo que es interesante referise a el en casos de problemas con el correcto
funcionamiento del aplicativo.

	{"reqId":"2zV7AbeG6wlZtxGREvx3","level":2,"time":"2018-05-02T08:08:34+00:00","remoteAddr":"172.17.0.1","user":"carlos","app":"core","method":"POST",
	"url":"\/login\/confirm","message":"Login failed: 'carlos' (Remote IP: '172.17.0.1')","userAgent":"Mozilla\/5.0 (X11; Fedora; Linux x86_64; rv:47.0) 
	Gecko\/20100101 Firefox\/47.0","version":"13.0.1.1"}

Dentro del directorio /config/ podemos encontrar el fichero de configuración de Nextcloud en el que se almacena la información necesaria para el funcionamiendo del aplicativo pero gran parte de esta está indexada en la base de datos por lo que la configuración de conexión con esta es crucial.

	$CONFIG = array (
	  'htaccess.RewriteBase' => '/',
	  'memcache.local' => '\\OC\\Memcache\\APCu',
	  'apps_paths' =>
	  array (
	    0 =>
	    array (
	      'path' => '/var/www/html/apps',
	      'url' => '/apps',
	      'writable' => false,
	    ),
	    1 =>
	    array (
	      'path' => '/var/www/html/custom_apps',
	      'url' => '/custom_apps',
	      'writable' => true,
	    ),
	  ),
	  'instanceid' => 'occlsju9eejl',
	  'passwordsalt' => 'zBZpjrS92PLOt6iCnxpE5aGnulJ9cq',
	  'secret' => 'Ins5cPZpRiR61jSghCWkcCmyEmq/mwHe+eP86N0HIPaJ6lu7',
	  'trusted_domains' =>
	  array (
	    0 => 'localhost:8080',
	  ),
	  'datadirectory' => '/var/www/html/data',
	  'overwrite.cli.url' => 'http://localhost:8080',
	  'dbtype' => 'mysql',
	  'version' => '13.0.1.1',
	  'dbname' => 'nextcloud',
	  'dbhost' => '172.17.0.2',
	  'dbport' => '',
	  'dbtableprefix' => 'oc_',
	  'dbuser' => 'oc_Admin',
	  'dbpassword' => '2Zumi6p3GdpYWcs9PNuSAMyU+KaD5q',
	  'installed' => true,
	  'ldapIgnoreNamingRules' => false,
	  'ldapProviderFactory' => '\\OCA\\User_LDAP\\LDAPProviderFactory',
	);
	
**Trusted Domains**

Al realizar pruebas de conexión a Nextcloud desde otra máquina diferente a 'localhost' hemos detectado que este no confía en la procedencia de la conexión y por ello no
permite el acceso.

Para solventar este problema basta por añadir toda la red interna en el fichero de configuración de Nextcloud de la siguiente forma:

	...
	'trusted_domains' => 
	  array (
		0 => 'localhost:8080',
		1 => '192.168.*.*',
	  ),
	...

#### Indexación de documentos

Tal y como se ha indicado previamente, Nextcloud indexa en la base de datos la configuración y información de los usuarios. La estructura de tablas de la base de datos que utiliza Nextcloud es la siguiente:

	+-----------------------------+
	| Tables_in_nextcloud         |
	+-----------------------------+
	| oc_accounts                 |
	| oc_activity                 |
	| oc_activity_mq              |
	| oc_addressbookchanges       |
	| oc_addressbooks             |
	| oc_admin_sections           |
	| oc_admin_settings           |
	| oc_appconfig                |
	| oc_authtoken                |
	| oc_bruteforce_attempts      |
	| oc_calendarchanges          |
	| oc_calendarobjects          |
	| oc_calendarobjects_props    |
	| oc_calendars                |
	| oc_calendarsubscriptions    |
	| oc_cards                    |
	| oc_cards_properties         |
	| oc_comments                 |
	| oc_comments_read_markers    |
	| oc_credentials              |
	| oc_dav_shares               |
	| oc_federated_reshares       |
	| oc_file_locks               |
	| oc_filecache                |
	| oc_files_trash              |
	| oc_flow_checks              |
	| oc_flow_operations          |
	| oc_group_admin              |
	| oc_group_user               |
	| oc_groups                   |
	| oc_jobs                     |
	| oc_ldap_group_mapping       |
	| oc_ldap_group_members       |
	| oc_ldap_user_mapping        |
	| oc_migrations               |
	| oc_mimetypes                |
	| oc_mounts                   |
	| oc_notifications            |
	| oc_notifications_pushtokens |
	| oc_oauth2_access_tokens     |
	| oc_oauth2_clients           |
	| oc_personal_sections        |
	| oc_personal_settings        |
	| oc_preferences              |
	| oc_properties               |
	| oc_schedulingobjects        |
	| oc_share                    |
	| oc_share_external           |
	| oc_storages                 |
	| oc_systemtag                |
	| oc_systemtag_group          |
	| oc_systemtag_object_mapping |
	| oc_trusted_servers          |
	| oc_twofactor_backupcodes    |
	| oc_users                    |
	| oc_vcategory                |
	| oc_vcategory_to_object      |
	+-----------------------------+

Concretamente es interesante revisar la tabla "oc_ldap_user_mapping" en la que se almacena el mapeado de usuarios de LDAP que ha detectado Nextcloud. Aquí podemos comprobar la asignación DN -> UUID con la que se creará el directorio del usuario cuando este haga login por primera vez.

	+------------------------------------------+--------------------------------------+--------------------------------------+
	| ldap_dn                                  | owncloud_name                        | directory_uuid                       |
	+------------------------------------------+--------------------------------------+--------------------------------------+
	| cn=pau pou,ou=usuaris,dc=edt,dc=org      | 5d078276-95ee-1037-8d35-db438ea12f92 | 5d078276-95ee-1037-8d35-db438ea12f92 |
	| cn=pere pou,ou=usuaris,dc=edt,dc=org     | 5d079b58-95ee-1037-8d36-db438ea12f92 | 5d079b58-95ee-1037-8d36-db438ea12f92 |
	| cn=anna pou,ou=usuaris,dc=edt,dc=org     | 5d07b76e-95ee-1037-8d37-db438ea12f92 | 5d07b76e-95ee-1037-8d37-db438ea12f92 |
	| cn=marta mas,ou=usuaris,dc=edt,dc=org    | 5d07de92-95ee-1037-8d38-db438ea12f92 | 5d07de92-95ee-1037-8d38-db438ea12f92 |
	| cn=jordi mas,ou=usuaris,dc=edt,dc=org    | 5d07fb48-95ee-1037-8d39-db438ea12f92 | 5d07fb48-95ee-1037-8d39-db438ea12f92 |
	| cn=admin system,ou=usuaris,dc=edt,dc=org | 5d081ed4-95ee-1037-8d3a-db438ea12f92 | 5d081ed4-95ee-1037-8d3a-db438ea12f92 |
	| cn=user01,ou=usuaris,dc=edt,dc=org       | 5d083964-95ee-1037-8d3b-db438ea12f92 | 5d083964-95ee-1037-8d3b-db438ea12f92 |
	| cn=user02,ou=usuaris,dc=edt,dc=org       | 5d0856ce-95ee-1037-8d3c-db438ea12f92 | 5d0856ce-95ee-1037-8d3c-db438ea12f92 |
	| cn=user03,ou=usuaris,dc=edt,dc=org       | 5d0880ea-95ee-1037-8d3d-db438ea12f92 | 5d0880ea-95ee-1037-8d3d-db438ea12f92 |
	| cn=user04,ou=usuaris,dc=edt,dc=org       | 5d08aeee-95ee-1037-8d3e-db438ea12f92 | 5d08aeee-95ee-1037-8d3e-db438ea12f92 |
	| cn=user05,ou=usuaris,dc=edt,dc=org       | 5d0ab7ca-95ee-1037-8d3f-db438ea12f92 | 5d0ab7ca-95ee-1037-8d3f-db438ea12f92 |
	| cn=user06,ou=usuaris,dc=edt,dc=org       | 5d0ac9b8-95ee-1037-8d40-db438ea12f92 | 5d0ac9b8-95ee-1037-8d40-db438ea12f92 |
	| cn=user07,ou=usuaris,dc=edt,dc=org       | 5d0aeb8c-95ee-1037-8d41-db438ea12f92 | 5d0aeb8c-95ee-1037-8d41-db438ea12f92 |
	| cn=user08,ou=usuaris,dc=edt,dc=org       | 5d0b0446-95ee-1037-8d42-db438ea12f92 | 5d0b0446-95ee-1037-8d42-db438ea12f92 |
	| cn=user09,ou=usuaris,dc=edt,dc=org       | 5d0b12c4-95ee-1037-8d43-db438ea12f92 | 5d0b12c4-95ee-1037-8d43-db438ea12f92 |
	| cn=user10,ou=usuaris,dc=edt,dc=org       | 5d0b2642-95ee-1037-8d44-db438ea12f92 | 5d0b2642-95ee-1037-8d44-db438ea12f92 |
	+------------------------------------------+--------------------------------------+--------------------------------------+
	
También es relevante consultar la tabla "oc_appconfig" en la que, junto con otra configuración, podemos encontrar la información de la configuración de LDAP.

	...
	| user_ldap               | ldap_agent_password                  | anVwaXRlcg==                                                                                                                                                                                                                                           |
	| user_ldap               | ldap_base                            | dc=edt,dc=org                                                                                                                                                                                                                                          |
	| user_ldap               | ldap_base_groups                     | ou=usuaris,dc=edt,dc=org                                                                                                                                                                                                                               |
	| user_ldap               | ldap_base_users                      | ou=usuaris,dc=edt,dc=org                                                                                                                                                                                                                               |
	| user_ldap               | ldap_configuration_active            | 1                                                                                                                                                                                                                                                      |
	| user_ldap               | ldap_display_name                    | cn                                                                                                                                                                                                                                                     |
	| user_ldap               | ldap_dn                              | cn=Manager,dc=edt,dc=org                                                                                                                                                                                                                               |
	| user_ldap               | ldap_email_attr                      | mail                                                                                                                                                                                                                                                   |
	| user_ldap               | ldap_group_filter                    | (&(|(objectclass=posixGroup)))                                                                                                                                                                                                                         |
	| user_ldap               | ldap_group_filter_mode               | 0                                                                                                                                                                                                                                                      |
	| user_ldap               | ldap_group_member_assoc_attribute    | gidNumber                                                                                                                                                                                                                                              |
	| user_ldap               | ldap_groupfilter_objectclass         | posixGroup                                                                                                                                                                                                                                             |
	| user_ldap               | ldap_host                            | ldapserver                                                                                                                                                                                                                                             |
	| user_ldap               | ldap_login_filter                    | (&(|(objectclass=inetOrgPerson))(|(uid=%uid)(|(cn=%uid))))                                                                                                                                                                                             |
	| user_ldap               | ldap_loginfilter_attributes          | cn                                                                                                                                                                                                                                                     |
	| user_ldap               | ldap_loginfilter_username            | 1                                                                                                                                                                                                                                                      |
	| user_ldap               | ldap_port                            | 389                                                                                                                                                                                                                                                    |
	| user_ldap               | ldap_tls                             | 0                                                                                                                                                                                                                                                      |
	| user_ldap               | ldap_turn_off_cert_check             | 1                                                                                                                                                                                                                                                      |
	| user_ldap               | ldap_userfilter_objectclass          | inetOrgPerson                                                                                                                                                                                                                                          |
	| user_ldap               | ldap_userlist_filter                 | (|(objectclass=inetOrgPerson))                                                                                                          
	...

Nextcloud utiliza una base de datos para indexar los documentos y poder realizar búsquedas de estos. Desde la versión 11, integra opcionalmente
[fulltextsearch](https://nextcloud.com/blog/nextcloud-11-introduces-full-text-search/) el cual permite realizar búsquedas de texto incluso dentro de estos ficheros.

He investigado la posibilidad de [utilizar Elastic Search en Nextcloud](https://lime-technology.com/forums/topic/69992-request-elastic-search/) pero no veo
estabilidad en los intentos que se han hecho por lo que decido no atacar esta etapa hasta tener establecidas unas bases del proyecto.

#### Creación del dockerfile de Nextcloud

Después de varias pruebas, se han solucionado los errores que se han ido encontrando para acabar con un Dockerfile el cual creada la imagen abre un contenedor que comunica con la base de datos y LDAP y permite autenticar usuarios. Los erroes encontrados han sido los siguientes:

* **Desconocimento de la ubicación de la configuración de LDAP.**

Tras investigar las tablas de la base de datos se encontró que toda la configuración de LDAP está en la tabla oc_appconfig citada anteriormente.

* **Problemas de configuración de hosts** (tratando de copiar el archivo de hosts a su destino).

Docker no permite cargar un fichero de hosts, hay que cargarlos después de la construcción del contenedor. Posteriormente se decidió trabajar con IPs por lo que esta configuración se ha eliminado.

* **Problemas de asignación de propiedad y permisos de los directorios /data/ y /config/.**

Al arrancar los contenedores se comprobó que durante la construcción, por las modificaciones realizadas, los contenedores /data/ y /config/ eran de root por lo que al arranque el usuario de apache no podía utilizarlos.

Simplemente ajustando estos permisos en el script de inicio se solucionó el problema.

* **Conexión de Nextcloud con la base de datos.**

Realizando manualente la configuración de Nextcloud por primera vez, este solicita en usuario administrador de la base de datos el cual utiliza para crear su propio usuario con privilegios restringidos para la base de datos de Nextcloud y el cual configura en el fichero config.php.

Principalmente, para evitar todo el proceso de configuración de Nextcloud se pensó en cargar un dump de la base de datos information_schema pero al tratarse de una base de datos de sistema [no es posible](https://stackoverflow.com/questions/10976371/how-to-restore-the-information-schema-database-with-a-backup) solucionarlo de esta manera.

Finalmente se optó por cargar un volcado de la base de datos de nextcloud, la cual contiene toda la configuración necesaria prestablecida, i para poder gestionar esta, se añade el usuario administrador indicando los permisos necesarios todo esto en el arranque de la base de datos.

	# @edt ASIX M14 2018
	# dmc:nextcloud
	# Servidor nextcloud
	# ----------------------------------
	FROM nextcloud:latest								-> partimos de la ultima imagen oficial de Nextcloud.

	COPY ./config.php /var/www/html/config/config.php	-> Copiamos nuestro fichero de configuración general.
	RUN chmod 750 config/config.php						-> Configuramos los permisos del fichero de configuración.
	COPY ./.ocdata /var/www/html/data/.ocdata			-> Copiamos un fichero sample '.ocdata' (es necesario)
	RUN mkdir /opt/docker								-> Creamos el directorio donde copiaremos nuestro fichero de startup.
	COPY ./startup.sh /opt/docker						-> Copiamos nuestro fichero de startup.
	CMD ["/opt/docker/startup.sh"]						-> Ejecutamos el fichero startup.sh.
	EXPOSE 80											-> Exponemos el puerto 80.



	#Fichero de startup para Nextcloud
	#Author: José Luis Pérez
	#DMC Project 2018

	chown -R www-data:www-data /var/www/html			-> Cambiamos propiedad de todo el directorio /var/www/html
	apache2-foreground									-> Ejecutamos en foreground apache.

Adicionalmente, para el proceso de investigación de esta etapa se han aplicado conocimientos de [Nextcloud CLI](https://docs.nextcloud.com/server/9/admin_manual/configuration_server/occ_command.html) y 
[LDAP configuration API](https://docs.nextcloud.com/server/13/admin_manual/configuration_user/user_auth_ldap_api.html) probando y listando las configuraciones de LDAP por linea de comandos.

### Transmisión de audio/video bajo demanda

En primera instancia se pensaba utilizar PLEX Media para dar servicio a la compartición de video y audio por streaming pero tras una pequeña investigación
este parece ser no compatible con LDAP. Puede encontrarse más información en el siguiente [enlace oficial](https://forums.plex.tv/discussion/269370/ldap-for-plex):

> No LDAP or any other type of network login support is currently available and I don't see this on the horizon either as it would probably need substantial changes to the various code spaces to support such a feature. Each user would essentially have to create their own account and have a unique email address associated with their account which could be a business domain email address.
> When ever someone is let go or hired you would have to delete and or add an account for them with Plex. How workable this is would depend on the size of the business.

Buscando una alternativa y encuentro las siguientes:

* Streamview: Clon indéntico a Netflix pero resulta ser software *open souce* de pago.
* Opencast: Software enfocado a la grabación, procesado y publicación de contenido.
* Emby: Buena solución que soporta con ldap pero no veo info de como indexa ficheros.
* Ampache: Solución webui de streaming de audio/video que soporta ldap y mysql.

#### Entorno de pruebas

De la búsqueda realizada para remplazar a PLEX, decido proner a prueba Opencast y ampache.

**Opencast**

Para llevar a cabo la instalación del entorno de pruebas de Opencast se han utilizado el docker compose "allinone" que nos pone a nuestra disposición Opencast. Para ello simplemente tenemos que seguir los siguientes pasos:

1. Clonamos el contenedor con el '.yml' que utilizaremos.

	https://github.com/opencast/opencast-docker

2. Nos situamos en el directorio del '.yml' y arrancamos el compose.

	cd [directorio]
	export HOSTIP=<IP address of the Docker host>
	docker-compose -p opencast-allinone -f docker-compose.allinone.h2.yml up

Seguidamente se realizaron varias pruebas de funcionamiento del aplicativo y se optó por descargarlo como una opción valida dado al alto tiempo de procesado del contenido.

https://hub.docker.com/r/opencast/allinone/

https://docs.opencast.org/develop/admin/configuration/security.ldap/

**Ampache**

Ampache es un reproductor en streaming de video y audio el cual trabaja con una base de datos mysql para la indexación del contenido (tal y como hace Nextcloud también) por lo que haremos uso del mismo contenedor que Nextcloud.

Tras varias pruebas de funcionamiento, se opta por utilizar ampache ya que cumple con todos los requisitos (reproduce video y audio en streaming, compatible con ldap, utiliza mysql para indexar datos).

Para instalar el entorno de pruebas de Ampache de la misma forma se ha utilizado un contenedor de MySQL oficial y uno de Ampache. Para ello simplemente tenemos que seguir los siguientes pasos:

* **Creamos el contenedor de Ampache**

	docker run --name=ampache -d -v /path/to/your/media:/media:ro -p 80:80 ampache/ampache

* **Configuramos Ampache**

Previo a la configuración, dado que se va a utilizar el contenedor de MySQL, creamos un usuario para ampache manualmente en el propio contenedor:

	CREATE DATABASE ampache; (luego el configurador la sobreescribirá)
	CREATE USER 'amp_adm'@'%' IDENTIFIED BY 'amppass';
	GRANT ALL ON ampache.* TO 'amp_adm'@'%';

Para configurar ampache se ha seguido el configurador web el cual se accede mediante la url:

	http://localhost

Es importante en este paso que introduzcamos los valores correctos de la configuración de la base de datos, el resto a nuestro gusto.

![](../Imágenes/ampache_db_config.png)

#### Configuración de LDAP

Antes de aplicar configuraciones se ha instalado el módulo de PHP para LDAP ya que realizando pruebas hemos visto que no venía instalado y resulta necesario

	apt-get -y install php5-ldap

Para que ampache autentique contra nuestro LDAP, tenemos que configurar ciertas directivas del fichero de configuración del cual puede obtenerse más información en el siguiente [enlace](https://github.com/ampache/ampache/wiki/LDAP). Estas directivas son las siguientes:

	...
	auth_methods = "ldap"
	...
	auto_create = "true"
	...
	auto_user = "user"
	...
	ldap_url = "ldap://172.10.10.3/"
	ldap_username = "cn=Manager,dc=edt,dc=org"
	ldap_password = "secret"
	ldap_search_dn = "dc=edt,dc=org"
	ldap_objectclass = "posixAccount"
	ldap_filter = "(uid=%v)"
	ldap_name_field = "uid"
	...

El resto de configuraciones serían opcionales pero aplicando estas ya sería suficiente para poder iniciar sesión con los usuarios de LDAP.

#### Personalización del theme

Dada la modularidad de ampache, es posible utilizar temas personalizados copiando y modificando el original. Una prueba que se llevo a cabo se hizo mezclando ficheros de un [tema personalizado](https://github.com/YannickArmspach/Ampache-theme-flat-white) con el tema por defecto y el resultado fue el siguiente:

	materialnew/
	|-- images
	|-- preview.png
	|-- templates
	\-- theme.cfg.php

![](../Imágenes/ampache_theme.png)

Este nuevo tema estará disponible para ser usado por los usuarios desde el menú configuración > Interfaz.

Como es comprensible, multiples aspectos de la personalización pueden ser modificados directamente el la base de datos directamente, en concreto en la tabla preference.

#### Carga del catálogo de audio y video

Para realizar la cargada de contenido disponible tanto para audio como para video creamos un nuevo catalogo desde la columna de gestión en el menú de catálogo > Añadir catálogo y indicamos los datos solicitados, por ejemplo:

![](../Imágenes/ampache_catalog_creation.png)

Esta acción nos está añadiendo nueva información a la base de datos de ampache, podemos consultarla en las tablas catalog, catalog_local, entre otras.

	+----+----------+--------------+-------------+------------+------------+---------+----------------+--------------+----------------+
	| id | name     | catalog_type | last_update | last_clean | last_add   | enabled | rename_pattern | sort_pattern | gather_types   |
	+----+----------+--------------+-------------+------------+------------+---------+----------------+--------------+----------------+
	|  1 | Musica   | local        |           0 |       NULL | 1525766805 |       1 | %T - %t        | %a/%A        | music          |
	|  7 | Personal | local        |           0 |       NULL | 1525767469 |       1 | %T - %t        | %a/%A        | personal_video |
	+----+----------+--------------+-------------+------------+------------+---------+----------------+--------------+----------------+

	+----+-------------------------+------------+
	| id | path                    | catalog_id |
	+----+-------------------------+------------+
	|  1 | /media/audio            |          1 |
	|  5 | /media/video/personales |          7 |
	+----+-------------------------+------------+
	
#### Carga de feeds de radio

Ampache también ofrece la posiblidad de cargar feeds de radio para ser escuchados directamente desde el aplicativo. para ello el usuario administrador tiene que añadir en radio las estaciones para que posteriormente puedan ser
escuchadas por los usuarios.

![](../Imágenes/ampache_add_radio.png)

El codec utilizado principalmente para los feeds de radio ha sido mp3 ya que otros como 'm3u' o 'ogg' han dado problemas en las pruebas de uso.
	
#### Análisis de tráfico

Para llegar a esta conclusión se ha partido de la idea de que el streaming hace uso de UDP y así ha sido las dos últimas decadas pero, 
tras una extensa investigación al respecto, se ha encontrado que gracias a la popularización del streaming y las mejoras implementadas para TCP, 
este ya no presenta un problema para ser utilizado para estos fines.

Servicios como Youtube, Hulu o Dailymotion entre otros hacen uso de HTTP/TCP por lo que el correcto funcionamiento de este modo de ofrecer video bajo demanda
está más que respaldado por grandes corporaciones.

Para corroborar el uso de este modo de streaming que utiliza Ampache se ha hecho una captura de tráfico en la que podemos ver que este genera tráfico HTTP/TCP y MySQL. Más concretamente podemos determinar los paquetes por:

![](../Imágenes/wireshark_pcap.png)

* MySQL: Ampache hace hace las consultas a la base de datos preguntando por valoraciones, cátalogo, status, entre otros y recibe la respuesta.

Algunos ejemplos de consultas de los paquetes son los siguientes:

	SELECT * FROM `album` WHERE `id`='1'
	SELECT * FROM song_data WHERE `song_id` = '5'
	SELECT `object_id` FROM `user_flag` WHERE `user` = '3' AND `object_id` IN (0,1,2,3,4,5,6,7) AND `object_type` = 'song'

* TCP: en el que podemos ver el *payload* que se está enviando en segmentos.

![](../Imágenes/wireshark_pcap_payload.png)

Otra prueba captura de tráfico que se ha llevado a cabo es durante la escucha de un stream de radio. Para ello se ha utilizado Wireshark escuchando el bridge de la red de la infraestructura
de la misma forma que se hizo anteriormente.

![](../Imágenes/wireshark_pcap_radio.png)

Podemos ver que igual que en la anterior captura, las tramas transmitidas viajan por TCP.

Estas son las pruebas finales de reproducción de audio y video que se han llevado a cabo:

![](../Imágenes/ampache_aud_demo.png)

![](../Imágenes/ampache_vid_demo.png)

#### Modos de transmisión de contenido

Ampache cuenta con varias formas de reproducir el contenido que tenemos indexado en el aplicativo. Estos modos se encuentran en el desplegable de la 
esquina superior derecha y son las siguientes:

**Flujo**

Este modo se caracteríza por el hecho de delegar la reproducción del contenido por streaming en un reproductor local como VLC descargando un fichero '.m3u' el cual
en el momento que sea abierto por nuestro reproductor empezará a reproducir el archivo en cuestión.

Como se ha indicado previamente, un ejemplo de reproductor es VLC el cual puede instalarse ejecutando las siguientes ordenes:

	$> su -
    #> dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    #> dnf install vlc
    #> dnf install python-vlc npapi-vlc (optionals)

**Reproductor Local**

Este modo de reproducción, como el propio modo indica, reproduce por los medios localmente conectados a Ampache. En nuestro caso no podemos hacer uso de este ya que
no contamos con medios para probar este modo.

**Democrático**

Probablemente uno de los modos más peculiares, consiste en organizar una lista de reproducción por los votos que realicen los usuarios. Similar a las listas de reproducción
compartidas de Spotify o Youtube.

**Reproductor Web**

Modo principal y por defecto de Ampache el cual utiliza un reproductor web HTML5 para mostrar el contenido.

Durante las pruebas de uso nos hemos dado cuenta que podría ser necesario tener que instalar los siguientes paquetes para que la reproducción de video funcione correctamente:

	dnf -y install ffmpeg ffmpeg-compat

#### Creación del contenedor final

Para la creación del contenedor final, se ha decidido partir del contenedor oficial aplicandole las modificaciones oportunas para cumplir los requisitos.

De la misma forma que en el caso de Nextcloud, es necesario realizar el volcado de la base de datos y cargarlo en la construcción del contenedor de MySQL. De esta forma
cargaríamos toda la configuración inicial necesaria y el arranque sería productivo desde el primer momento.

Para ello volcaríamos la base de datos de la siguiente forma:

	mysqldump -u root -h 172.10.10.2 -p ampache > ampache.sql

A continuación añadimos la configuración necesaria al contenedor de MySQL quedando tal que así:

	vim Dockerfile
		# @edt ASIX M14 2018
		# dmc:mysql
		# Servidor mysql
		# ----------------------------------
		FROM mysql:5.6

		RUN mkdir /opt/docker
		COPY ./nextcloud.sql /opt/docker
		COPY ./ampache.sql /opt/docker
		COPY ./startup.sh /opt/docker
		RUN chmod 775 /opt/docker/startup.sh
		ENV MYSQL_ROOT_PASSWORD jupiter
		CMD ["/opt/docker/startup.sh"]
		EXPOSE 3306



	vim startup.sh
		service mysql start
		mysql -u root -e "CREATE USER 'root'@'%' IDENTIFIED BY 'jupiter';"
		mysql -u root -e "GRANT ALL ON *.* TO 'root'@'%';"
		mysql -h localhost -u root < /opt/docker/nextcloud.sql
		mysql -u root -e "CREATE USER 'oc_Admin'@'%' IDENTIFIED BY '2Zumi6p3GdpYWcs9PNuSAMyU+KaD5q';"
		mysql -u root -e "GRANT ALL PRIVILEGES ON nextcloud . * TO 'oc_Admin'@'%';"
		mysql -h localhost -u root < /opt/docker/ampache.sql
		mysql -u root -e "CREATE USER 'amp_adm'@'%' IDENTIFIED BY 'amppass';"
		mysql -u root -e "GRANT ALL PRIVILEGES ON ampache . * TO 'amp_adm'@'%';"
		service mysql stop
		/bin/sh /usr/bin/mysqld_safe

Finalmente tenemos que construir el Dockerfile de Ampache cargando el fichero de configuración preconfigurado:

	cat Dockerfile
		# @edt ASIX M14 2018
		# dmc:nextcloud
		# Servidor nextcloud
		# ----------------------------------
		FROM ampache/ampache:latest

		RUN apt-get update && apt-get -y install php5-ldap
		COPY ./ampache.cfg.php /var/www/config/ampache.cfg.php
		RUN chown www-data /var/www/config/ampache.cfg.php
		RUN mkdir /var/log/ampache
		CMD ["/usr/sbin/apachectl","-e info -DFOREGROUND"]
		EXPOSE 80

Es importante prestar atención a los permisos de los ficheros copiados y a las configuraciones de conexión con la base de datos ya han aparecido problemas durante
las pruebas de arranque del nuevo contenedor y se han resuelto investigando directamente accediendo al contenedor y realizando todo el proceso de arranque manualmente.

Finalizada y comprobada la construcción del contenedor de Ampache, es hora de añadir este al fichero de compose, para ello se ha añadido la siguiente configuración.
También en este punto se han añadido los volumenes ya que tanto Nextcloud como Ampache tienen que hacer uso de ellos.

	[...]
	  #Nextcloud
	  nextcloud:
		build: 
		  context: $PWD/nextcloud
		  dockerfile: Dockerfile
		ports:
		  - "8080:80"
		networks: 
		  _net:
			ipv4_address: 172.10.10.4
		container_name: dmc_nextcloud
		hostname: nextcloudserver
		**volumes:**
		  **- type: volume**
			**source: nexdata**
			**target: /var/www/html**

	  #Ampache
	  ampache:
		build:
		  context: $PWD/ampache
		  dockerfile: Dockerfile
		ports:
		  - "80:80"
		networks:
		  _net:
			ipv4_address: 172.10.10.5
		container_name: dmc_ampache
		hostname: ampacheserver
		**volumes:**
		  **- /home/users/inf/hisx2/isx47240077/Downloads:/media:ro**
		#privileged: true

	networks:
	 _net:
	   driver: bridge
	   ipam:
		config:
		  - subnet: 172.10.10.0/24

	volumes:
	 nexdata:


Como se puede ver, se han utilizado dos tipos de volumenes (volume y bind).

En el caso del tipo volume, concretamente named volume, este se le indica un nombre de volumen y el directorio de montado y docker se encarga de crear dicho volumen
en el directorio "/var/lib/docker/volumes/contenedores_nexdata/" para asegurar la persistencia de los datos de este directorio.

Para el caso de ampache, al tratarse de un volumen de tipo bind, estamos montando el directorio "/home/users/inf/hisx2/isx47240077/Downloads" de la máquina host en "/media"
en el contenedor en modo lectura. De esta forma ampache puede hacer la carga de contenido a los catálogos sin tener permiso de escritura en este directorio o subdirectorios.
Listar ips contenedores docker: docker ps -q | xargs -n 1 docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} {{ .Name }}' | sed 's/ \// /'

https://github.com/Kickball/awesome-selfhosted#video-streaming

### Video-llamadas en directo

Para hacer frente a las video-llamadas, principalmente pretendía utilizar JITSI pero por motivos de carga y tiempo se ha optado finalmente por Nextcloud Talk.
Este es un módulo de Nextcloud basado en la librería simpleWebRTC que permite realizar video-llamdas desde el mismo aplicativo.

Para activar dicho módulo es tan simple como dirigirnos como usuario administrador al apartado de "Apps" y localizar en "Multimedia" el módulo "Talk". A continuación con que
hagamos click en "Enable" ya tendríamos completamente operativo el módulo y podríamos realizar llamdas entre los usuarios de LDAP.

![](../Imágenes/nex_talk_module.png)

Se ha llevado a cabo la siguiente prueba en la que se ha realizado una llamada triple con Anna Pou, Pere Pou y user01 (solo Anna tenía cámara web) y se ha analizado el
tráfico que genera la llamada. Este es el siguiente:

![](../Imágenes/nex_talk_vidcall.png)

![](../Imágenes/nex_talk_pcap.png)


